import networkx as nx
import csv

g = nx.DiGraph()

# name	abbreviation	category	open	size
with open("trackers.csv") as f:
    reader = csv.reader(f)

    next(reader)
    for row in reader:

        name, abbreviation, category, is_open, size = row
        g.add_node(row[0], abbreviation=abbreviation, category=category, open=bool(is_open), size=int(size))

# source    requirement target
with open("edges.csv") as f:
    reader = csv.reader(f)

    next(reader)
    for row in reader:
        g.add_edge(row[0], row[2], req=row[1])


nx.write_gml(g, "network.gml")
